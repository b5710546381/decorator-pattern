Decorator Pattern
=======

1. Description
----------
Decorator design pattern is used to enhance the functionality of an individual object at run-time or dynamically. At the same time other instances of the same class will not be affected by this.

Here is a UML diagram showing the relationship between classes when applied with decorator pattern.
![decorator_simple.jpg](https://bitbucket.org/repo/a9jX8o/images/3369483177-decorator_simple.jpg)

When to use this pattern:

1. There are too many subclass with very minimal difference through inheritance.
2. When an individual object only needs to add more functions at run-time.
3. When we want to improve a class that has too much functions
4. There is no need to go back and change every class if there is something to be changed.

Pros and Cons:

Pros:

1. Reduce too many class needed to be create because it can add functions at run-time.
2. Can progressively add functionality (We can apply this pattern more than once on the same object).

Cons:

1. Can only apply on an individual object at run-time.

2. Example source code
----------

We want to create a window that show text, but when the text is too much to show it will be problematic so we will add a scroll bar in window to be able scroll down the text.

```
#!java
//Create an interface for Window
public interface Window {
	public void renderWindow();
}

//Create a concrete component that implements Window

public class SimpleWindow implements Window {
	@Override
	public void renderWindow() {
		// rendering details
	}
}

//Create abstract decorator that implement Window
public abstract class DecoratedWindow implements Window {
	// private reference to decorating Window object
	private Window windowReference = null;
	public DecoratedWindow( Window windowRef ) {
		this.windowReference = windowRef;
	}
	@Override
	public void renderWindow() {
		windowReference.renderWindow();
	}
	private void renderScrollBar() {
		//render scroll bar
	}
}

//Create concrete decorator that will add functionality to the concrete component.
public class ScrollableWindow extends DecoratedWindow{

	private Object scrollBarObjectRepresentation = null;
	
	public ScrollableWindow(Window windowRefernce) {

		super(windowRefernce);
	}
	
	@Override
	public void renderWindow() {
	
		// render scroll bar 
		renderScrollBarObject();
		
		// render decorated window
		super.renderWindow();
	}

	private void renderScrollBarObject() {

		// prepare scroll bar 
		scrollBarObjectRepresentation = new  Object();
		
		
		// render scrollbar 
		
	}	
}

/*Create Main to run the program. At start, the text will probably not 
be too much for the Window to show. As text increases, Window object 
can't show all of them. We check first if the text is too much for the 
Window to show. If it is then we apply the decorator to Window object 
which it will add a scroll bar to Window object*/
public class Main {
	public static void main( String[] args ) {
		Window window = new ConcreteWindow();
		wndow.renderWindow();
		//check when text becomes bigger than window
		window = new ScrollableWindow( window );
		//render again
		window.render
	}
}
```

3. Exercise
----------

Please apply a decorator pattern on these block of code

```
#!Java

public abstract class Pizza {
	public abstract void setDetail( String name );
	public abstract double getPrice();
	public abstract String getDetail();
}

public class DoubleChessePizza extends Pizza {
	@Override
	public void setDetail() {
		
	}
	@Override
	public String getDetail() {
		return "Parmesan, Mozzarella, Chesse Pizza";
	}
	@Override getPrice() {
		return 200.0;
	}
}

public class TripleChessePizza extends Pizza {
	@Override
	public void setDetail() {
	
	}
	@Override
	public String getDetail() {
		return "Parmesan, Mozzarella, Fontina, Chesse Pizza";
	}
	@Override
	public double getPrice() {
		return 299.0;
	}
}

//and many other recipes of pizza
```

When you have done the exercise, go check the solution [here](https://bitbucket.org/b5710546381/decorator-pattern/src/0626dcee48f11a9514fe41ba2f0e06dd63a29a43/ExerciseSolution.md?at=master)