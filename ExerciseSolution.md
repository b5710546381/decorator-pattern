The solution to this problem is to decorate a pizza again and again so there is no need to create
all recipes of pizza class which will cause class explosion and we can further create topping as much as we want.

```
#!java

public interface Pizza {
    public String getDetail();
    public double getPrice();
}

public class SimplePizza implements Pizza {
    
    @Override
    public String getDetail() {
        return "Simple Dought";
    }
    public double getPrice() {
        return 150.0;
    }
    
}

public abstract class ToppingDecorator implements Pizza {
    protected Pizza pizzaReference;
    public ToppingDecorator( Pizza pizzaRef ) {
        pizzaReference = pizzaRef;
    }
    public String getDetail() {
        return pizzaReference.getDetail();
    }
    public double getPrice() {
        return pizzaReference.getPrice();
    }
}
//Example of concrete decorators
public class Paremesan extends ToppingDecorator {
    
    public Parmesan( Pizza pizzaRef ) {
        super( pizzaRef );
    }
    public String getDetail() {
        return pizzaReference.getDetail() + "with Parmesan";
    }
    public double getCost() {
        return pizzaReference.getPrice() + 100.0;
    }
}

public class Seafood extends ToppingDecorator {
    
    public Seafood( Pizza pizzaRef ) {
        super( pizzaRef );
    }
    public String getDetail() {
        return pizzaReference.getDetail() + "with Seafood";
    }
    public double getCost() {
        return pizzaReference.getPrice() + 150.0;
    }
}
```